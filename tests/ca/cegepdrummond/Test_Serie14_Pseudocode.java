package ca.cegepdrummond;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie14_Pseudocode extends SimulConsole{
    @Test
    @Order(1)
    void test_pseudo1() throws Exception {
        choixMenu("14a");
        ecrire("3 0");
        ecrire("0 4");
        ecrire("0 0");
        assertSortie("carré", false);
        
        choixMenu(".");
        ecrire("11 0");
        ecrire("0 60");
        ecrire("0 0");
        assertSortie("carré", false);

        choixMenu(".");
        ecrire("3 0");
        ecrire("0 4");
        ecrire("1 1");
        assertSortie("quelconque", false);

        choixMenu(".");
        ecrire("10 0");
        ecrire("0 10");
        ecrire("0 0");
        assertSortie("carré", false);
        
    }


    @Test
    @Order(2)
    void test_pseudo2() throws Exception {
        choixMenu("14b");
        ecrire("0 4 4 0 9 6 8 5 7");
        assertSortie("valide", false);

        choixMenu(".");
        ecrire("0 4 6 4 5 4 2 8 6");
        assertSortie("valide", false);

        choixMenu(".");
        ecrire("1 2 3 4 5 6 7 8 2");
        assertSortie("valide", false);

        choixMenu(".");
        ecrire("0 4 4 0 9 6 8 5 6");
        assertSortie("invalide", false);

        choixMenu(".");
        ecrire("0 4 6 4 5 4 2 8 7");
        assertSortie("invalide", false);
    }


}
